## build and install gem
gem build bom_builder.gemspec
gem install bom_builder-0.0.0.gem 

## usage
bom_builder [PATH_TO_PROJECT]

**Output:** bom.xml file in project directory

## Description
This script can be used to convert Gemfile.lock to a bom file (example below)

bom stands for `bill of materials` and can used to describe the components that are packaged with an application.

The  file can be exported to [dependency track](https://dependencytrack.org/) which is (as the name implies) a tool that can keep track of project dependencies for security vulnerabilites. 

* It has a dashboard to check what vulnerabilites a project has and how severe they are. 

* Supports notification with slack. 

* Dependencies from different frameworks (i.e. ROR and Vue.js) can be uploaded as well. 

example output: 
```
<?xml version="1.0" encoding="UTF-8"?>
<bom xmlns="http://cyclonedx.org/schema/bom/1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1" xsi:schemaLocation="http://cyclonedx.org/schema/bom/1.0 http://cyclonedx.org/schema/bom/1.0">
  <components>
    <component type="library">
      <name>Ascii85</name>
      <version>1.0.3</version>
      <description>Ascii85 encoder/decoder</description>
      <hashes>
        <hash alg="SHA-256">7ae3f2eb83ef5962016802caf0ce7db500c1cc25f385877f6ec64a29cfa8a818</hash>
      </hashes>
      <purl>pkg:gem/Ascii85@1.0.3</purl>
      <modified>false</modified>
    </component>
  </components>
</bom>
```